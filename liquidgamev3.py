import os
import re
import asyncio
import random
import telepot
import string
import telegram
import sqlite3
from telegram.ext import ApplicationBuilder, CommandHandler, MessageHandler, filters, Application, CallbackQueryHandler, \
    ContextTypes
from telegram.ext import CallbackContext
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
import gspread
import pyperclip
import time
from datetime import datetime
from telegram import Update
from oauth2client.service_account import ServiceAccountCredentials
import pandas as pd
import matplotlib.pyplot as plt
from telegram.ext.filters import MessageFilter
from PIL import Image, ImageDraw, ImageFont
from googleapiclient.discovery import build
from google.oauth2 import service_account
from googleapiclient.errors import HttpError
import logging
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from emoji import emojize
from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

# Set up logging
log_file = 'logs.log'
formatter = logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)

file_handler = logging.FileHandler(log_file)
file_handler.setFormatter(formatter)



# Google Sheets details
SPREADSHEET_NAME = 'New game'
SHEET_NAME_LEADERBOARD = 'Leaders'
SCOPES = ['https://www.googleapis.com/auth/spreadsheets', 'https://www.googleapis.com/auth/drive']
SheetID = '1BNIrswoHb5AELpI-eEp-RKJvEZms9lQXtcMeoJe4cR4'

#Thadeus Bot token
#TELEGRAM_TOKEN = '6677231514:AAFphzDDQLlNdHYTdrSd6QbGV06a3hY-VDI'

#Liquid bot token
TELEGRAM_TOKEN = '6096097564:AAHf4f03fOKdkk73y3jJlXB_nXCZXrmVpPA'
SHEET_NAME_SHEET1 = 'Sheet1'

pid = os.getpid()
token = TELEGRAM_TOKEN
SCOPES = ['https://www.googleapis.com/auth/spreadsheets', 'https://www.googleapis.com/auth/drive']
credentials_file = 'keys.json'
credentials = service_account.Credentials.from_service_account_file(credentials_file, scopes=SCOPES)
SheetID = '1BNIrswoHb5AELpI-eEp-RKJvEZms9lQXtcMeoJe4cR4'
service = build('sheets', 'v4', credentials=credentials)
sheet = service.spreadsheets()

creds2 = service_account.Credentials.from_service_account_file('keys.json', scopes=SCOPES)

# Authorize the credentials and create a client to interact with the API
client = gspread.authorize(creds2)

sheet_id = '1BNIrswoHb5AELpI-eEp-RKJvEZms9lQXtcMeoJe4cR4'

# Create or connect to an SQLite database
conn = sqlite3.connect('referral2.db')
cursor = conn.cursor()

game_started = {}
global_counter = 1

DATABASE_URL ="sqlite:///referral2.db"
engine = create_engine(DATABASE_URL)
Base = declarative_base()

class Referral(Base):
    __tablename__ = "referrals"
    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, unique=True, index=True)
    referral_code = Column(String, unique=True, index=True)
    referral_count = Column(Integer, default=0)
    #share_count = Column(Integer, default=0)
    
    
Base.metadata.create_all(bind=engine)


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    user_id = update.effective_user.id

    
    """Sends a message with inline buttons attached."""
    keyboard = [

        [
            InlineKeyboardButton("Play", callback_data="survivor"), 
            InlineKeyboardButton("Instructions", callback_data="instructions"),
            InlineKeyboardButton("Terms", callback_data="faq"),
        ],
        [
            InlineKeyboardButton("Leaderboard", callback_data="leaderboard"),
            InlineKeyboardButton("Referrals", callback_data="referrals"),         
            InlineKeyboardButton("Wallet", callback_data="balance"),
        ],
        [
            InlineKeyboardButton("Discuss", callback_data="discussion"),
            InlineKeyboardButton("Trade", callback_data="shop"),
            InlineKeyboardButton("Earnings", callback_data="earning"),
        ],
    ]

    reply_markup = InlineKeyboardMarkup(keyboard)

    await update.message.reply_text("Please choose:", reply_markup=reply_markup)
    #db_session.close()
    

async def send_main_menu(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    user_id = update.effective_user.id

    """Sends a message with inline buttons attached."""
    keyboard = [

        [
            InlineKeyboardButton("Play", callback_data="survivor"), 
            InlineKeyboardButton("Instructions", callback_data="instructions"),
            InlineKeyboardButton("Terms", callback_data="terms"),
        ],
        [
            InlineKeyboardButton("Leaderboard", callback_data="leaderboard"),
            InlineKeyboardButton("Referrals", callback_data="referrals"),         
            InlineKeyboardButton("Wallet", callback_data="balance"),
        ],
        [
            InlineKeyboardButton("Discuss", callback_data="discussion"), 
            InlineKeyboardButton("Trade", callback_data="shop"),
            InlineKeyboardButton("Earnings", callback_data="earning"),
        ],
    ]

    reply_markup = InlineKeyboardMarkup(keyboard)
    
    if update.callback_query:
        await update.callback_query.message.edit_text("Please Choose:", reply_markup=reply_markup)
    else:

        await update.message.reply_text("Please choose:", reply_markup=reply_markup)

# Define your game instructions or about text as a string
game_instructions = """You have made it inside the Liquid Markets Game! 🎉🚀

Here is a jaw-dropping offer for you! 🤩✨ Liquids objective is to provide for free to as many talented traders as possible, a fair real house account with raw spreads and 2.56 round turn commissions, no liability for loss, yet with guaranteed payouts, as well as the doubling of your balance and profitability, for achieving simple ROI targets in a manner that allows Liquid to comfortably copy some of your trades with a few extra bullets!

🕹This game is a simple look at your decision making ability in a zero sum game environment and also reward you with cash!🎯 🤔 or fast track you into our proven path to fund manage our house account.

💹Can you outsmart 99 strangers over five days with equal information❓ If you can, we will reward you with $100 and a 🆓 100k prop account to further test your survival skills in the markets***

There are 5️⃣ rounds for this game. Each round is open from 00:00 to 23:59 every week day and it takes just 1 minute to play each round. 
The following is the information you will need to win. 

⚡️Liquid crowdsource funds, they need to price their first demo: Liquid 100.  
⚡️You are to help by submitting a bid from numbers ranging from 1-100.  
⚡️The average number picked by 100 players will be market fair value. 
⚡️The fundamental cost to Liquid is 30% of each unit.

As you are part of the market, the only thing you need to do is to bid the number that you think will be 70% of the fair value. The final 70% of the average  is referred to as the settlement price and will be published along with the days final leaderboard.
🔒 The closer your bid is to the settlement, the more points you receive each day. On Friday you will receive the $100,000 account which in turn leads to Nitro house fund management account. This top prize or others depends on your final accumulated scores. 
🔥 Do not explain or discuss these instructions with others. It is to your advantage to know these instructions better than the others. 

First come first serve. Play every week till you win $100 or $100k account. Get started!!! Observe your rivals but watch your back! remember that its a zero sum game💸 If you need a friend get a dog!!!"""

game_about = """ **Terms and Conditions for Liquid Game on Telegram**

By participating in this game, you agree to abide by the following terms and conditions. Please read them carefully before taking part.\n

Eligibility:
1. Players must be at least 18 years old and have a valid Telegram account to participate in the Liquid Game. Players must be registered and verified at liquidmarkets.org site.\n 

Gameplay Period:
2. The game is played every week from Monday to Friday, with gameplay periods beginning at 00:00 and 23:59 (GMT daily) Game sessions end every 24hours from Monday to Friday\n

Player Limit:
3. Each session, a maximum, referred to as the ‘cohort’ number of players are allowed receive points in the weekly game. This cohort will be multiples of 100.\n

Referral System:
4. Participants may refer friends to join the game using their unique referral code. Referrers earn bonus points for successful referrals. Every referred player who goes on to buy or renew a Liquid trading plan within 6 months will be accredited to the referrers ‘Affiliate’ account with Liquid.\n

Scoring System:
5. Players earn Energy points for estimating the eventual settlement value in a range of numbers during the gameplay period. Energy points are also earned for referrals, participation and leadership. 

The scoring system for the Game is as follows:

– Settlement value: 100 Points
– Incremental proximity to the settlement value: +/- 0.001 Points
– Time priority scoring is applied to incremental scores

Prize Distribution:
6. Prizes will be awarded to the top 10 players with the highest energy scores at the end of the gameplay week (Friday 1900 GMT). Prizes include gifts with varying values grouped in up to 5 levels.

Prize Levels:
7. Players may only win each prize level once. If a player has already won a specific level prize in the past, they are eligible for only a higher level prize .

Prize Redemption:
8. Prizes won in the game are not transferable to other players or individuals. Players can redeem their prizes for wallet credits with Liquid Markets only. 7 days are provided for winners to claim prizes.

Disqualification:
9. Liquid Markets reserves the right to disqualify any player found to be engaging in unfair practices, cheating, or violating these terms and conditions. The game rules and instructions may not be discussed in any public group or forum. Players may not request extra help, direction or instruction from previous, current or future players or from Liquid staff or operatives. Requesting extra help will result in disqualification. 

Changes and Termination:
10. Liquid Markets may modify, suspend, or terminate the Liquid Game at its discretion, without prior notice.

Privacy Policy:
11. By participating in the game, players agree to the collection and use of their personal information as outlined in the Liquid Markets Privacy Policy https://liquidmarkets.org/privacy-2/.

Disclaimer:
12. Liquid Markets is not responsible for any technical issues, interruptions, or network failures that may affect gameplay.

Governing Law:
13. These terms and conditions are governed by and construed in accordance with the laws of the United Kingdom.

Claims:
14. Prizes are awarded inside the Liquid dashboard. Therefore every player must connect their Telegram Id to their Liquid account before claiming. Selfie ID is required.

Contact us
14. If you have any questions or concerns regarding the game or these terms and conditions, please contact us at gamemaster@liquidmarkets.org

By participating in the Liquid Game on Telegram, you acknowledge that you have read, understood, and agreed to these terms and conditions. Failure to comply with these terms may result in disqualification from the game. Good luck and enjoy the game!

Every winner in the top 10 on a weekly basis will receive a free prop path to Nitro house account, via academy accounts or via demo challenge accounts. $100 or a Liquid 100 Account that leads to house Nitro will be awarded to the player with the highest score at the end of the every gameplay week (Saturday 13:00 GMT).
 """ 


async def button(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    def generate_referral_code(user_id: int) -> str:
        random.seed(user_id)
        code = ''.join(random.choices(string.ascii_uppercase + string.digits, k=6))
        return code
    
    async def handle_referrals(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        user_id = update.callback_query.from_user.id

        referral_message = f"Join Liquid Game using my link: liquidmarkets.org/liquidgame?ref={user_id}. Copy this verification code for use in  registration of Liquid Game 24/11 Week: {user_id}"
        
        keyboard = [[InlineKeyboardButton("Share", switch_inline_query=referral_message)],
                    [InlineKeyboardButton("Copy Verication code", callback_data="copy")],
                     [InlineKeyboardButton("Back to Main Menu", callback_data="home")]]
        reply_markup = InlineKeyboardMarkup(keyboard)
        await query.edit_message_text(text=referral_message, reply_markup=reply_markup)
        return user_id
        
    async def copy_verification_code(update, context):
        user_id = update.callback_query.from_user.id
        pyperclip.copy(str(user_id))
        await update.callback_query.answer(text="Verification code copied!")



    # Add code to retrieve and display the wallet information
    async def handle_discussion(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        keyboard = [[InlineKeyboardButton("Back to Main Menu", callback_data="home")]]
        reply_markup = InlineKeyboardMarkup(keyboard)
        discussion_group_link = "https://t.me/liquidmarkets" 
        await query.edit_message_text(text=f"Join our discussion group: {discussion_group_link}", reply_markup=reply_markup)

    # Add code to handle the redirection to the discussion bot

    async def handle_instructions(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        keyboard = [[InlineKeyboardButton("Back to Main Menu", callback_data="home")]]
        reply_markup = InlineKeyboardMarkup(keyboard)
        await query.edit_message_text(text=game_instructions, reply_markup=reply_markup)

    # Add code to retrieve and display the wallet information
    async def handle_faq(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        keyboard = [[InlineKeyboardButton("Back to Main Menu", callback_data="home")]]
        reply_markup = InlineKeyboardMarkup(keyboard)
        await query.edit_message_text(text=game_about, reply_markup=reply_markup)

    # Add code to handle the redirection to the discussion bot

    async def handle_shop(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        shop_details = f"Trade with us on: liquidmarkets.org/shop"
        keyboard = [[InlineKeyboardButton("Back to Main Menu", callback_data="home")]]
        reply_markup = InlineKeyboardMarkup(keyboard)
        await query.edit_message_text(text=f"{shop_details}", reply_markup=reply_markup)
        
        
    async def handle_earning(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        keyboard = [[InlineKeyboardButton("Back to Main Menu", callback_data="home")]]
        reply_markup = InlineKeyboardMarkup(keyboard)
        await query.edit_message_text(
            text=f"There are 4 ways to acquire Liquid Energy (l.joules):\n"
            "1. Winning Energy: Your top scoring session will account for 70% of your total liquid energy measured in l.joules.\n"
            "2. Refferals Energy: The number of new referrals you bring during the weekly game will account for 15% of your total liquid energy points measured in l.joules.\n"
            "3. Participation Energy: The number of game periods you participate in will account for 5% of your total liquid energy points measured in l.joules.\n"
            "4. Leadership Energy: Your leadership energy accounts for 10% of your total energy. Leadership energy is simply your referrals multiplied by their participation energy."
            ,reply_markup=reply_markup)

    # Add code to retrieve and display the wallet information

    async def handle_leaderboard(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        
        try:
            conn1 = sqlite3.connect('referral2.db')
            cursor1 = conn1.cursor()

            user_id = update.callback_query.from_user.id
            keyboard = [[InlineKeyboardButton("Back to Main Menu", callback_data="home")]]
            reply_markup = InlineKeyboardMarkup(keyboard)
            # retrieve_and_insert_data(global_counter)
            leaderboard_group_link = "https://t\.me/liquidmarkets"
            username = update.callback_query.from_user
            
            # Check if the user_id is available in the used_by column in referrals table
            cursor1.execute("SELECT referred_by FROM referrals WHERE user_id=?", (user_id,))
            referral_users = cursor.fetchall()  # Fetch all users who used the same user_id

            # Calculate the values for Game, Energy, and Referrals
            cursor1.execute("SELECT MAX(highest_score) FROM result_summary1 WHERE user_id=?", (user_id,))
            existing_user = cursor1.fetchone()
            score = existing_user[0] if existing_user else 0
            if score is not None:
                score = min(score, 80000)
            else:
                score = 0

            cursor1.execute("SELECT MAX(num_plays) FROM plays WHERE user_id=?", (user_id, ))
            existing_user = cursor1.fetchone()
            game = existing_user[0] if existing_user else 0
            if game is not None:  # Check if game is not None before performing operations
                game = min(game * 5, 75)
            else:
                # Define what should happen if game is None, maybe set it to a default value
                game = 0 

            cursor1.execute("SELECT MAX(referral_count) FROM referrals WHERE user_id=?", (user_id,))
            referral = cursor1.fetchone()
            referrals = referral[0] if referral else 0
            if referrals is not None:
                referrals = min(referrals * 300, 15000)
            else:
                referrals = 0
            
            
            sql_queries = []

            # Retrieve the used_by values from the referrals table
            cursor1.execute("SELECT referred_by FROM referrals WHERE user_id = ?", (user_id,))
            used_by_values = cursor1.fetchall()
                    
            # Extracting the string from the tuple
            user_id_string = used_by_values[0][0]

            # Splitting the string to get individual IDs, excluding 'None'
            if user_id_string is not None:
                # user_ids = user_id_string
                user_ids = [user_id_string]

                for uid in user_ids:
                    # Create SQL queries for each user_id
                    sql_queries.append(
                        f"""
                        SELECT MAX(CASE WHEN p.game > 15 THEN 15 ELSE p.game END) AS highest_game,
                            MAX(p.score) AS highest_score,
                            MAX(CASE WHEN p.num_plays > 15 THEN 15 ELSE p.num_plays END) AS highest_num_plays
                        FROM plays AS p
                        WHERE p.user_id = {uid}
                        """
                    )

                # Construct the final query by joining individual queries using UNION ALL
                final_query = "SELECT SUM(highest_num_plays) AS total_highest_num_plays FROM ("
                final_query += " UNION ALL ".join(sql_queries)
                final_query += ") AS subquery;"

                
                # Execute the query
                cursor1.execute(final_query)

                # Fetch the result
                result = cursor1.fetchone()

                # Extract the total highest num plays
                total_highest_num_plays = result[0]
                
                if total_highest_num_plays is not None:
                    referral_energy = min(total_highest_num_plays * 60, 4500)
                else:
                    referral_energy = 0

                # Commit the changes and close the database connection
                conn1.commit()
                conn1.close()

                # Define the leaderboard data
                leaderboard_data = [
                    ("Game", score),
                    ("Energy", game),
                    ("Referrals", referrals),
                    ("Referrals Energy", referral_energy)
                ]

                # Calculate the total score
                total_score = sum(score for _, score in leaderboard_data)
                
                # Format the data as a Markdown table
                leaderboard_table = "```SCORES\n"
                for item, score in leaderboard_data:
                    leaderboard_table += f"{item:<16} | {score}\n"
                leaderboard_table += f"Total            | {total_score}\n```"
                await query.edit_message_text(text=f"Hello {username.first_name},  here are your Leaderboard scores: {leaderboard_table} \n"
                                            , parse_mode="MarkdownV2", reply_markup=reply_markup)
                
        except sqlite3.Error as e:
            # Handle database errors
            print(f"SQLite error: {e}")

        except Exception as ex:
            # Handle other exceptions
            print(f"Error: {ex}")

        finally:
            if 'conn1' in locals():
                conn1.close()
        # Add code to retrieve and display the wallet information

    """Parses the CallbackQuery and updates the message text."""
    query = update.callback_query

    await query.answer()

    # if query.data == "survivor":
    # await handle_join_survivor(update, context)

    if query.data == "instructions":
        await handle_instructions(update, context)
    elif query.data == "shop":
        await handle_shop(update, context)
    elif query.data == "copy":
        await copy_verification_code(update, context)
    elif query.data == "referrals":
        await handle_referrals(update, context)
    elif query.data == "leaderboard":
        await handle_leaderboard(update, context)
    elif query.data == "discussion":
        await handle_discussion(update, context)
    elif query.data == "terms":
        await handle_faq(update, context)
        
    elif query.data == "home":
        await send_main_menu(update, context)
        
    elif query.data == "earning":
        await handle_earning(update, context)

    elif query.data == "balance":
        user = query.from_user
        user_id = user.id
        
        db_session = sessionmaker(bind=engine)()
        referral = db_session.query(Referral).filter_by(user_id=user_id).first()
        
        if referral:
            referral_count = referral.referral_count
        else:
            referral_count = 0
        
        keyboard = [[InlineKeyboardButton("Back to Main Menu", callback_data="home")]]
        reply_markup = InlineKeyboardMarkup(keyboard)
        
        try:
            conn2 = sqlite3.connect('referral2.db')
            cursor2 = conn2.cursor()
        
            # Check if the user_id is available in the used_by column in referrals table
            cursor2.execute("SELECT referred_by FROM referrals WHERE user_id=?", (user_id,))
            referral_users = cursor2.fetchall()  # Fetch all users who used the same user_id

            # Calculate the values for Game, Energy, and Referrals
            cursor2.execute("SELECT MAX(highest_score) FROM result_summary1 WHERE user_id=?", (user_id,))
            existing_user = cursor2.fetchone()
            score = existing_user[0] if existing_user else 0
            if score is not None:
                score = min(score, 80000)
            else:
                score = 0

            cursor2.execute("SELECT MAX(num_plays) FROM plays WHERE user_id=?", (user_id, ))
            existing_user = cursor2.fetchone()
            game = existing_user[0] if existing_user else 0
            if game is not None:  
                game = min(game * 5, 75)
            else:
                game = 0 

            cursor2.execute("SELECT referral_count FROM referrals WHERE user_id=?", (user_id,))
            referral = cursor2.fetchone()
            referrals = referral[0] if referral else 0
            if referrals is not None:
                referrals = min(referrals * 300, 15000)
            else:
                referrals = 0
            
            
            sql_queries = []

            # Retrieve the used_by values from the referrals table
            cursor2.execute("SELECT referred_by FROM referrals WHERE user_id = ?", (user_id,))
            used_by_values = cursor2.fetchall()
                    
            # Extracting the string from the tuple
            user_id_string = used_by_values[0][0]

            # Splitting the string to get individual IDs, excluding 'None'
            if user_id_string is not None:
                # user_ids = user_id_string
                user_ids = [user_id_string]

                for uid in user_ids:
                    # Create SQL queries for each user_id
                    sql_queries.append(
                        f"""
                        SELECT MAX(CASE WHEN p.game > 15 THEN 15 ELSE p.game END) AS highest_game,
                            MAX(p.score) AS highest_score,
                            MAX(CASE WHEN p.num_plays > 15 THEN 15 ELSE p.num_plays END) AS highest_num_plays
                        FROM plays AS p
                        WHERE p.user_id = {uid}
                        """
                    )

                # Construct the final query by joining individual queries using UNION ALL
                final_query = "SELECT SUM(highest_num_plays) AS total_highest_num_plays FROM ("
                final_query += " UNION ALL ".join(sql_queries)
                final_query += ") AS subquery;"

                
                # Execute the query
                cursor2.execute(final_query)

                # Fetch the result
                result = cursor2.fetchone()
                print(result)

                # Extract the total highest num plays
                total_highest_num_plays = result[0]
                if total_highest_num_plays is not None:
                    referral_energy = min(total_highest_num_plays * 60, 4500)
                else:
                    referral_energy = 0

                # Commit the changes and close the database connection
                conn2.commit()
                conn2.close()
            
                total_energy = game + referrals + referral_energy
                
                await query.edit_message_text(
                    text=f"Hi: {user.first_name} , \nYour Game Energy is:  {score} \n Total Referral Energy: {total_energy}" 
                    ,reply_markup=reply_markup)
        
        
        except sqlite3.Error as e:
            # Handle database errors
            print(f"SQLite error: {e}")

        except Exception as ex:
            # Handle other exceptions
            print(f"Error: {ex}")

        finally:
            if 'conn2' in locals():
                conn2.close()
        
            
    elif query.data == "survivor":
        
        try:
            conn3 = sqlite3.connect('referral2.db')
            cursor3 = conn3.cursor()
        
            user_id = query.from_user.id
            user = query.from_user
            # Check if the user is registered in the database
            cursor3.execute("SELECT telegram_id FROM users WHERE telegram_id=?", (user_id,))
            existing_user = cursor3.fetchone()
            print("User is:", existing_user)
            
            registration_bot_url = "https://t.me/liquidgamebot"
            
            if not existing_user:
 
                game_started[user_id] = True  # Set game_started to True for the user

                await context.bot.send_message(user_id, f"You need to register to play the Liquid Game. Use this link to register: {registration_bot_url}")
            else:
                cursor3.execute("SELECT referral_count FROM referrals WHERE user_id=?", (user_id,))
                referral_data = cursor3.fetchone()
                
                conn3.commit()
                conn3.close()
                
                if referral_data and referral_data[0] >= 0:
                    game_started[user_id] = True  # Set game_started to True for the user
                    await query.message.edit_text(
                        text="Welcome to Liquid Game Weekely Challenge!\n\n"
                             "Joining the current 100 player round of the Liquid game!\n"
                             "Submit your bid price between 1 and 100 to continue.")
                
                else:
                    await context.bot.send_message(user_id, "You need to refer a player by sharing the Liquid Game.\n"
                                "Use '/start' to return to the main menu and refer a friend to the Liquid Game using 'Referrals' button.\n"
                                f"Then use the verication code copied from  here {registration_bot_url} to join the current Game(17/11).")

        except sqlite3.Error as e:
            # Handle database errors
            print(f"SQLite error: {e}")

        except Exception as ex:
            # Handle other exceptions
            print(f"Error: {ex}")

        finally:
            if 'conn3' in locals():
                conn3.close()

        # else:
        # await query.edit_message_text(text="Invalid button response."
        # await query.edit_message_text(text=f"Register to enable  {query.data}")




class CustomCommandFilter(MessageFilter):
    def __init__(self, command: str, prefix: str):
        super().__init__()
        self.command = command.lower()
        self.prefix = prefix

    def filter(self, message: Update) -> bool:
        return message.text.startswith(self.prefix + self.command)


emoji_party_popper = emojize(':party_popper:')
emoji_clapping_hands = emojize(':clapping_hands:')
cheers_emoji = emojize(":clinking_glasses:")
thumbs_up = emojize(':thumbs_up:')





class SalesBot:
    def __init__(self):
        self.user_names = {}  # Dictionary to store user_id: user_name pairs
        self.counter = 1
        self.round_finished = False
        self.guess_list = {}
        self.users_guess = list()
        self.all_game_ids = list()
        self.is_scheduled_results_running = False 
        global game_started
        self.game_started = game_started
        self.previous_counter = 0
        self.registered_users = set()  # Store registered user IDs



    async def start(self, update, context):
        user_id = update.effective_user.id
        user_name = update.effective_user.username or update.effective_user.first_name

        if not user_name:
            await update.message.reply_text("Please set a username or first name in your Telegram profile.")
            return

        # Check if the user is registered in the database
        cursor.execute("SELECT telegram_id FROM users WHERE telegram_id=?", (user_id,))
        existing_user = cursor.fetchone()
        print("User is:", existing_user)
        if not existing_user:
            # User is not registered, direct them to the registration bot
            registration_bot_url = "https://t.me/liquidgamebot"
            await update.message.reply_text(f"Register first to play the game. Use this link to register: {registration_bot_url}")
            return

        # User is registered, proceed with the game
        self.user_names[user_id] = user_name
        self.game_started[user_id] = True  # Update the game_started dictionary
        await update.message.reply_text("Welcome to Liquid Game!\n\n"
                                        "Joining the current round of the liquid game!\n"
                                        "Submit your bid price between 1 and 100 to continue.")

                

    async def retrieve_and_insert_data(self):
        global global_counter  # Ensure you're modifying the global variable

        sheet_index = self.counter
        # Increment the global_counter if sheet_index increased
        if sheet_index > global_counter:
            global_counter = sheet_index

        # Open your Google Sheets spreadsheet by its title or URL
        sheet = client.open_by_key(sheet_id).get_worksheet(sheet_index - 1)

        # Retrieve data from the Google Sheet starting from row 4 (assuming headers are in row 3)
        data = sheet.get_all_values()
        
        # print(data)
        
        try:
            conn4 = sqlite3.connect('referral2.db')
            cursor4 = conn4.cursor()

            # Loop through the data and insert it into the SQLite database
            for row in data[3:]:  # Start from the fourth row
                user_id_str = row[0]  # Assuming user_id is in column A
                if user_id_str.strip():  # Check if user_id_str is not an empty string
                    user_id = int(user_id_str)
                    username = row[1]  # Assuming username is in column B
                    score = float(row[13])  # Assuming score is in column N

                    # Count occurrences of user_id in plays table
                    cursor4.execute("SELECT COUNT(user_id) FROM plays WHERE user_id=?", (user_id,))
                    num_plays_count = cursor4.fetchone()[0]
                    
                    # Update referral_count in referrals table based on matches with used_by
                    cursor4.execute("""
                        UPDATE referrals 
                        SET referral_count = (
                            SELECT COUNT(DISTINCT p.user_id)
                            FROM plays p
                            WHERE p.user_id = referrals.referred_by
                        )
                        WHERE referrals.referred_by = ?
                    """, (user_id,))


                    # Insert the data into the SQLite table with the count of plays
                    cursor4.execute("INSERT OR REPLACE INTO plays (game, user_id, score, username, num_plays) VALUES (?, ?, ?, ?, ?)",
                                    (global_counter, user_id, score, username, num_plays_count + 1))


            
            # Check the condition for the query selection based on global_counter
            if global_counter == 0:
                query = """
                    SELECT
                        p.user_id,
                        CASE WHEN p.game > 15 THEN 15 ELSE p.game END AS highest_game,
                        p.score AS highest_score,
                        CASE WHEN p.num_plays > 15 THEN 15 ELSE p.num_plays END AS highest_num_plays,
                        CASE WHEN r.referral_count > 15 THEN 15 ELSE r.referral_count END AS referral_count
                    FROM
                        plays AS p
                    LEFT JOIN
                        referrals AS r ON p.user_id = r.user_id
                    WHERE
                        p.game = ?
                    GROUP BY
                        p.user_id;
                """
                cursor4.execute(query, (global_counter,))

            if global_counter <= 15:
                query = """
                    SELECT
                        p.user_id,
                        MAX(CASE WHEN p.game > 15 THEN 15 ELSE p.game END) AS highest_game,
                        MAX(p.score) AS highest_score,
                        MAX(CASE WHEN p.num_plays > 15 THEN 15 ELSE p.num_plays END) AS highest_num_plays,
                        MAX(CASE WHEN r.referral_count > 15 THEN 15 ELSE r.referral_count END) AS referral_count
                    FROM
                        plays AS p
                    LEFT JOIN
                        referrals AS r ON p.user_id = r.user_id
                    GROUP BY
                        p.user_id;
                """
                cursor4.execute(query)

            # Fetch all the results
            results = cursor4.fetchall()

            cursor4.execute("CREATE TABLE IF NOT EXISTS result_summary1 (id INTEGER PRIMARY KEY, game INTEGER, username TEXT, user_id INTEGER, highest_game INTEGER, highest_score REAL, highest_num_plays INTEGER, referral_count INTEGER, total_score REAL)")


            # print("IDS",userr_ids)]
            
            # Insert the results into the new table
            for row in results:
                
                num_plays_value = row[4] if row[4] is not None else 0
                total_score = (row[1] * 5) + row[2] + (row[3] * 60) + (num_plays_value * 300)  # Calculate the total_score 
                
                game = global_counter  # Use the global_counter as the game value
                user_id, highest_game, highest_score, highest_num_plays, referral_count = row
                
                # Retrieve the username for the user_id from the plays table
                cursor4.execute("SELECT username FROM plays WHERE user_id = ?", (user_id,))
                username_result = cursor4.fetchone()
                username = username_result[0] if username_result else None  # Get the username from the result
            
                cursor4.execute("SELECT MAX(num_plays) FROM plays WHERE user_id=?", (user_id, ))
                existing_user = cursor4.fetchone()
                game_score = existing_user[0] if existing_user else 0
                game_score = min(game_score * 5, 75)

                cursor4.execute("SELECT referral_count FROM referrals WHERE user_id=?", (user_id,))
                referral = cursor4.fetchone()
                referrals = referral[0] if referral else 0
                if referrals is not None:
                    referrals = min(referrals * 300, 15000)
                
                
                sql_queries = []

                # Retrieve the used_by values from the referrals table
                cursor4.execute("SELECT referred_by FROM referrals WHERE user_id = ?", (user_id,))
                used_by_values = cursor4.fetchall()
                
                
                # user_ids = []         
                # Extracting the string from the tuple
                if not used_by_values:
                    
                    total_energy = game_score + referrals 
                    cursor4.execute("INSERT INTO result_summary1 (game, username, user_id, highest_game, highest_score, highest_num_plays, referral_count, total_score, total_energy) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                                (game, username, user_id, highest_game, highest_score, highest_num_plays, total_energy, total_score, total_energy))

                else:
                    user_id_string = used_by_values[0][0]
            
                    # Splitting the string to get individual IDs, excluding 'None'
                    if user_id_string is not None and isinstance(user_id_string, str):
                        user_ids = [int(user_id) for user_id in user_id_string.split(',') if user_id != 'None']
                        
                    
                    
                        for uid in user_ids:
                            # Create SQL queries for each user_id
                            sql_queries.append(
                                f"""
                                SELECT MAX(CASE WHEN p.game > 15 THEN 15 ELSE p.game END) AS highest_game,
                                    MAX(p.score) AS highest_score,
                                    MAX(CASE WHEN p.num_plays > 15 THEN 15 ELSE p.num_plays END) AS highest_num_plays
                                FROM plays AS p
                                WHERE p.user_id = {uid}
                                """
                            )

                        # Construct the final query by joining individual queries using UNION ALL
                        final_query = "SELECT SUM(highest_num_plays) AS total_highest_num_plays FROM ("
                        final_query += " UNION ALL ".join(sql_queries)
                        final_query += ") AS subquery;"

                        cursor4.execute(final_query)

                        # Fetch the result
                        result = cursor4.fetchone()

                        # Extract the total highest num plays
                        total_highest_num_plays = result[0]

                        if total_highest_num_plays is not None:
                            referral_energy = min(total_highest_num_plays * 60, 4500)
                        else:
                            referral_energy = 0

                        
                        #conn.commit()
                        
                        total_energy = game_score + referrals + referral_energy
                        
                

                        cursor4.execute("INSERT INTO result_summary1 (game, username, user_id, highest_game, highest_score, highest_num_plays, referral_count, total_score, total_energy) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                                    (game, username, user_id, highest_game, highest_score, highest_num_plays, total_energy, total_score, total_energy))
                    
                    total_energy = game_score + referrals 
                    cursor4.execute("INSERT INTO result_summary1 (game, username, user_id, highest_game, highest_score, highest_num_plays, referral_count, total_score, total_energy) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                                    (game, username, user_id, highest_game, highest_score, highest_num_plays, total_energy, total_score, total_energy))

            
            conn4.commit()
            await self.send_leaderboard()
            
            bot = telepot.Bot(TELEGRAM_TOKEN)
            cursor4.execute('SELECT user_id FROM plays WHERE game=?', (global_counter,))
            #telegram_ids = [row[0] for row in cursor4.fetchall()]
            telegram_ids = ['5690431799']
            
            
            if global_counter >= 0:
                skip_ids = ['5376981234','6498032176','7215489602','8123067541','9332164089',	
                            '4678125639','5981623405','3241768092','8456092317','2164879350','7018432695','3745619820']
                for user_id in telegram_ids:
                    if user_id in skip_ids:
                        continue
                    if global_counter < 5:
                        message = f"Hello Liquid Game Player!. Session {global_counter} has ended. The next session {global_counter + 1} begins now. Join Liquid group: https://t.me/liquidmarkets to see the full Leaderboard. Thank You for playing the Liquid Game!."
                        try:
                            bot.sendMessage(user_id, message)
                        except telepot.exception.TelegramError as e:
                            if "Bad Request: chat not found" in str(e):
                                print(f"Chat not found error for user_id1 {user_id}")
                            elif "Bad Request: message is too long" in str(e):
                                print(f"Message is too long")
                            else:
                                print(f"An error occurred: {e}")
                                
                    else:
                        message = f"Hello Liquid Game Player!. The FINAL SESSION {global_counter} of this Game Week(01/11) has ended. Join Liquid group: https://t.me/liquidmarkets to see when the first session of next Game Week will start. Thank You for playing the Liquid Game!."
                        try:
                            bot.sendMessage(user_id, message)
                        except telepot.exception.TelegramError as e:
                            if "Bad Request: chat not found" in str(e):
                                print(f"Chat not found error for user_id2 {user_id}")
                            elif "Bad Request: message is too long" in str(e):
                                print(f"Message is too long")
                            else:
                                print(f"An error occurred: {e}")
            
            
            conn4.commit()
            conn4.close()
            if global_counter < 5:
                self.counter += 1
                print("Next Stage....")
                
                with open('user_ids.txt', 'w') as file:
                    file.truncate(0)
            
            else:       
                self.game_started = {}
        
        except sqlite3.Error as e:
            # Handle database errors
            print(f"SQLite error: {e}")

        except Exception as ex:
            # Handle other exceptions
            print(f"Error: {ex}")

        finally:
            if 'conn4' in locals():
                conn4.close()
    
    
    async def send_leaderboard(self):
        print("Sending Leaderboard")
        bot = telepot.Bot(TELEGRAM_TOKEN)
        settlement_price = sheet.values().get(spreadsheetId=SheetID, range=f'Sheet{global_counter}!C1:C1').execute()
        settlement_price = settlement_price.get('values', [])
        settlement_price = settlement_price[0][0]

        try:
            conn5 = sqlite3.connect('referral2.db')
            cursor5 = conn5.cursor()

            cursor5.execute("""
                SELECT 
                    username,
                    MAX(game) AS max_game,
                    MAX(IFNULL(referral_count, 0)) AS max_referral_count,
                    MAX(total_score) AS max_total_score
                FROM result_summary1
                GROUP BY username
            """)

            results = cursor5.fetchall()
            print("Results:", results)

            if not results:
                bot.sendMessage('1017000702', "No results found for this game!")
                return

            sorted_results = sorted(results, key=lambda x: (-x[3], -x[2]))

            if global_counter < 5:
                leaderboard_message = f"Leaderboard for Liquid Game Session {global_counter}: \n   Settlement Price = {settlement_price} \n\n"
                num_players = 20
            else:
                leaderboard_message = f"Final Leaderboard For This Week's Game: \n   Settlement Price = {settlement_price} \n\n"
                num_players = 10

            # Generate the Markdown table
            rank_length = len(str(num_players))
            max_username_length = max(len(player_data[0]) for player_data in sorted_results)
            max_scores_length = max(len(str(player_data[3])) for player_data in sorted_results)
            max_energy_length = max(len(str(player_data[2])) for player_data in sorted_results)

            markdown_table = f"{leaderboard_message}"
            markdown_table += f"| Rank{' '*(rank_length - 5)} | Player{' '*(max_username_length - 22)} | Scores{' '*(max_scores_length - 7)} | Energy{' '*(max_energy_length - 6)} |\n"
            markdown_table += f"|{'-'*(rank_length+2)}|{'-'*(max_username_length+2)}|{'-'*(max_scores_length+2)}|{'-'*(max_energy_length+2)}|\n"

            for i, player_data in enumerate(sorted_results[:num_players], start=1):
                if global_counter < 5:
                    username, game, referral_count, total_score = player_data
                    markdown_table += f"| {i}.{' '*(rank_length - len(str(i)))} | {username}{' '*(max_username_length - len(username))} | {total_score}{' '*(max_scores_length - len(str(total_score)))} | {referral_count}{' '*(max_energy_length - len(str(referral_count)))} |\n"
                else:
                    username, max_game, max_referral_count, max_total_score = player_data
                    markdown_table += f"| {i}.{' '*(rank_length - len(str(i)))} | {username}{' '*(max_username_length - len(username))} | {max_total_score}{' '*(max_scores_length - len(str(max_total_score)))} | {max_referral_count}{' '*(max_energy_length - len(str(max_referral_count)))} |\n"

            bot.sendMessage('1017000702', markdown_table)

        except Exception as e:
            print(f"An error occurred: {e}")
        finally:
            conn5.close()


    async def update_variable(self): # Move to next sheet and send leaderboard
        user_id = sheet.values().get(spreadsheetId=SheetID, range=f'Sheet{self.counter}!A4:A100').execute()
        output_user_values = user_id.get('values', [])
        user_values = set(value[0] for value in output_user_values)
        for user_id in user_values:
            self.guess_list[int(user_id)] = []
            
        # await self.retrieve_and_post_updates()
        self.counter += 1
        self.users_guess = list()
        self.round_finished = False
        await asyncio.sleep(len(user_values) * 0.25)
        
        
    def save_entries_to_file(self, entries):
        with open('entries.txt', 'w') as file:
            file.write(str(entries))

    def load_entries_from_file(self):
        try:
            with open('entries.txt', 'r') as file:
                return int(file.read().strip())
        except FileNotFoundError:
            return None
        
   
            
    async def counting_rounds(self):
        try:
            with open('num_rounds.txt', 'r') as file:
                stored_counter, num_rounds = map(int, file.read().split())
            
            if stored_counter == self.counter:
                return num_rounds
        
        except FileNotFoundError:
            pass
        
        await asyncio.sleep(1)
        rounds = sheet.values().get(spreadsheetId=SheetID, range=f'Sheet{self.counter}!A1:A1').execute()
        num_rounds = int(rounds.get('values', [])[0][0][-1])
        
        with open('num_rounds.txt', 'w') as file:
            file.write(f"{self.counter} {num_rounds}")
        
        return num_rounds




    async def is_rounds_finished(self):
        rounds_nums = await self.counting_rounds()  # number of rounds in the stage
        unique_ids = set(value[0] for value in self.all_game_ids)  # list of unique players ID
        
        await asyncio.sleep(1)
        rounds_users = sheet.values().get(spreadsheetId=SheetID, range=f'Sheet{self.counter}!C4:C100').execute()
        rounds_users = rounds_users.get('values', [])
        
        entries = self.load_entries_from_file()
        if entries is None:
            # If the value is not in the file, retrieve it using the Google Sheets API as before
            entries = int(re.search(r'\d+', sheet.values().get(spreadsheetId=SheetID,
                                                              range=f'Sheet{self.counter}!A2:A2').execute().get(
                'values', [])[0][0]).group())

        # Save the value of 'entries' to the file
        self.save_entries_to_file(entries)
        
        # Calculate the sum of player guesses for the current round
        player_guess_sum = len(rounds_users)
        

        # Calculate the expected sum of player guesses based on the condition round_sums * entries == number of player guesses
        expected_guess_sum = rounds_nums * entries

        # Check if the sum of player guesses matches the expected sum to move on to the next stage
        if player_guess_sum == expected_guess_sum and len(unique_ids) != 0:
            self.round_finished = True
        else:
            self.round_finished = False

            
            
    def post_to_sheet(self, val):
        if self.counter <= 5:
            sheet_name = f"Sheet{self.counter}"
        else:
            sheet_name = SHEET_NAME_LEADERBOARD
        post = sheet.values().append(spreadsheetId=SheetID, range=f'{sheet_name}!A4:D100',
                                     valueInputOption="USER_ENTERED",
                                     body={'values': val}).execute()
        return post
        
        
        
    async def calculate_and_update_scores(self):
        #num_counter = self.counter if self.counter != 6 else 5
        num_counter = min(self.counter, 5)
        user_inputs = sheet.values().get(spreadsheetId=SheetID, range=f'Sheet{num_counter}!C4:C100').execute()
        user_inputs = user_inputs.get('values', [])
        sum_total = sheet.values().get(spreadsheetId=SheetID, range=f'Sheet{num_counter}!C1:C1').execute()
        sum_total = sum_total.get('values', [])
        
        if not user_inputs or not sum_total:
            return #No data available to calculate the scores
        
        try:
            user_inputs = [float(val[0]) for val in user_inputs]
            sum_total = float(sum_total[0][0])
            
            player_scores = []
            user_inputs_counts = {}
            for guess in user_inputs:
                if guess not in user_inputs_counts:
                    user_inputs_counts[guess] = 0
                else:
                    user_inputs_counts[guess] += 1
                reduction = 0.001 * user_inputs_counts[guess] # Reduce the score
                player_score = 70 - abs(guess - sum_total) - reduction
                player_scores.append(abs(player_score))
                
            values_to_update = [[score] for score in player_scores]
            
            update_range = f'Sheet{num_counter}!D4:D{len(player_scores) + 3}'
            request = sheet.values().update(
                spreadsheetId = SheetID,
                range=update_range,
                body={'values': values_to_update},
                valueInputOption='USER_ENTERED'
            )
            
            response = request.execute()
            if 'updatedRows' in response:
                print(f'Updated {response["updatedRows"]} rows with play scores.')
            else:
                print(f'Failed to update player scores.')
                
        except Exception as e:
            print(f'Error calculating and updating player scores: {e}')
            
            
    async def users_input(self, update: Update, context: CallbackContext):
        if update.message.chat.type != "private":
            return  # Skip processing messages from groups and channels

        username = update.message.from_user.username
        user_id = update.effective_chat.id
    
        if not self.game_started.get(user_id, False):
            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text="Please start the game with /start before submitting bids.")
            return
            
        if update.message.text == '//restart' and username == 'Jorlaa':
            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text=f"Restarting Round. Please Wait")
            self.counter -= 1
            # await self.scheduled_results(update, context)
            return

        if update.message.text == '//next_stage' and username == 'Jorlaa':
            # self.round_finished = True
            #await self.calculate_and_update_scores()
            await self.retrieve_and_insert_data()
            return

        if update.message.text == '//finish_round' and username == 'Jorlaa':
            #self.round_finished = True 
            self.counter += 14
            return
            
        if update.message.text == '//clear' and username == 'Jorlaa':
            #global_counter = self.counter
            await self.send_leaderboard()
            # await self.retrieve_and_insert_data()
            #self.counter += 4
            return

        # Check if it's a new round, then overwrite the user_ids.txt
        if self.counter > 1:
            with open('user_ids.txt', 'a') as file:
                pass

        #with open('user_ids.txt', 'a') as file:
            #pass

        user_id = update.effective_chat.id
        ids = await self.get_ids()
        check_list = []
        for each_id in ids:
            check_list.append(each_id)
        rounds_nums = await self.counting_rounds()
        
        entries = self.load_entries_from_file()
        if entries is None:
            # If the value is not in the file, retrieve it using the Google Sheets API as before
            entries = int(re.search(r'\d+', sheet.values().get(spreadsheetId=SheetID,
                                                              range=f'Sheet{self.counter}!A2:A2').execute().get(
                'values', [])[0][0]).group())

        # Save the value of 'entries' to the file
        self.save_entries_to_file(entries)
        
        await asyncio.sleep(1)
        user_entries = sheet.values().get(spreadsheetId=SheetID, range=f'Sheet{self.counter}!A4:A100').execute().get('values', [])
        user_entries_sum = len(user_entries ) + 1
        
        
        if str(user_id) in check_list  and check_list.count(str(user_id)) >= rounds_nums:
            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text=f"Only {rounds_nums} bids per Stage. This stage has {rounds_nums} rounds. Wait for the next Stage!")
            return
        else:
            message = update.message
            user_name = message.from_user.name
            if update.message.text.isnumeric():
                new_msg = int(update.message.text)
                #await asyncio.sleep(2)
                #await self.sheet_users_inputs()
                if 0 < new_msg <= 100:
                    try:
                        if new_msg not in self.guess_list[user_id]:
                            pass
                    except KeyError:
                        self.guess_list[user_id] = list()
                    if new_msg not in self.guess_list[user_id]:
                        self.guess_list[user_id].append(new_msg)
                        self.users_guess.append(new_msg)
                        try:
                            if user_entries_sum <= entries * rounds_nums:
                                self.post_to_sheet([[user_id, user_name, message.text]])
                                
                                # Append the user_id to a text file
                                with open('user_ids.txt', 'a') as file:
                                    file.write(str(user_id) + '\n')
                                    
                                await context.bot.send_message(chat_id=update.effective_chat.id,
                                                               text=f"Your bid of {new_msg} has been accepted{thumbs_up}. Check your place on the liquidmarkets leaderboard")
                        
                                await self.calculate_and_update_scores()
                                
                                #await context.bot.send_message(chat_id=update.effective_chat.id,
                                               #text=f"Stage {self.counter} update. {user_entries_sum}/{entries * rounds_nums} bids, Wait till the end of the round for your score and leaderboard place.")
                                               #text=f"Leaderboard {self.counter} update. {user_entries_sum}/{entries} bids, Wait till the end of the round for your score and leaderboard place.")

                
                        except Exception as e:
                            print(e)
                            await context.bot.send_message(chat_id=update.effective_chat.id,
                                                           text=f"liquid game is temporarily suspended !! CONTACT GAME MASTER")
                    else:
                        await context.bot.send_message(chat_id=update.effective_chat.id,
                                                       text=f"your guess is repetitious!!")
                else:
                    await context.bot.send_message(chat_id=update.effective_chat.id,
                                                   text=f"Your bid must be between 1 and 100 !")
            else:
                await context.bot.send_message(chat_id=update.effective_chat.id,
                                               text="try again, only unique bids accepted")

        # Call scheduled_results
        await asyncio.sleep(1)
        value_to_update = int(sheet.values().get(spreadsheetId=SheetID, range=f'Sheet{self.counter}!D1:D1').execute().get('values', [])[0][0])
        
        total_entries = entries * rounds_nums
        if user_entries_sum <= total_entries:
            if (user_entries_sum % value_to_update == 0 or user_entries_sum == total_entries or (total_entries - user_entries_sum) < value_to_update):
                
                if not self.is_scheduled_results_running:
                    self.is_scheduled_results_running = True
                    # await self.scheduled_results(update, context)
                    #self.reset_leaderboard()
                    self.is_scheduled_results_running = False
                
        else:
            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           text=f"You've reached the maximum number of bids for this round.")
                                           
                                           
            #await self.scheduled_results(update, context)                               
   
    async def get_ids(self):
        unique_ids = set()  # Initialize an empty set
        with open('user_ids.txt', 'r') as file:
            all_ids = [line.strip() for line in file.readlines()]
        
        unique_ids = list(set(all_ids))

        self.all_game_ids = unique_ids
       
        return unique_ids
         
        


    def reset_leaderboard(self, sheet_name='Leaders'):
        try:
            # Fetch the values within the specified range A2:C100
            values = sheet.values().get(spreadsheetId=SheetID, range=f'{sheet_name}!A2:C100').execute().get('values', [])

            # Create a list of empty rows to match the number of rows in the range
            empty_rows = [['' for _ in range(3)] for _ in range(len(values))]

            # Update the range with the empty rows to clear the values
            update_range = f'{sheet_name}!A2:C{len(values) + 1}'  # +1 because range is 1-indexed

            # Create the request to send to the Google Sheets API
            update_request = sheet.values().update(
                spreadsheetId=SheetID,
                range=update_range,
                body={'values': empty_rows},
                valueInputOption='RAW'
            )

            # Execute the update request
            update_request.execute()

            print("Leaderboard has been reset successfully.")
        except Exception as e:
            print(f"An error occurred while resetting the leaderboard: {e}")

            
            

            
    async def sheet_users_inputs(self):
        user_inputs = sheet.values().get(spreadsheetId=SheetID,
                                         range=f'Sheet{self.counter}!C4:C100').execute()
        self.users_guess = user_inputs.get('values', [])
        self.users_guess = list(int(guess[0]) for guess in self.users_guess)

            
    
    def get_results(self):
        num_counter = self.counter
        if self.counter == 6:
            num_counter = 5
        time_sheet = sheet.values().get(spreadsheetId=SheetID, range=f'Sheet{num_counter}!A3:A3').execute()
        target_time = time_sheet.get('values', [])[0][0]
        user_inputs = sheet.values().get(spreadsheetId=SheetID, range=f'Sheet{num_counter}!C4:C100').execute()
        user_inputs = user_inputs.get('values', [])
        sum_total = sheet.values().get(spreadsheetId=SheetID, range=f'Sheet{num_counter}!C1:C1').execute()
        sum = sum_total.get('values', [])
        target_time = re.findall(r'\b\d+\b', target_time)
        return target_time, user_inputs, sum


   
def run_bot():
    game_bot = SalesBot()

    scheduler = AsyncIOScheduler()
    scheduler.add_job(game_bot.retrieve_and_insert_data, 'interval', minutes=1440)
    scheduler.start()

    application = ApplicationBuilder().token(token).build()
    que = application.job_queue

    start_handler = CommandHandler('start', start)
    input_handler = MessageHandler(filters.Text(), game_bot.users_input)

    application.add_handler(CommandHandler("start", start))
    application.add_handler(CallbackQueryHandler(button))
    application.add_handler(CallbackQueryHandler(send_main_menu, pattern="home"))

    application.add_handler(start_handler)
    application.add_handler(input_handler)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(application.run_polling())



# def run_bot():
#     game_bot = SalesBot()
    
#     loop = asyncio.get_event_loop()
    
#     scheduler = AsyncIOScheduler()
#     scheduler.add_job(game_bot.retrieve_and_insert_data, 'interval', minutes=1440)
#     while True:
#         application = ApplicationBuilder().token(token).build()
#         que = application.job_queue
        
#         #start_handler = CommandHandler('start', game_bot.start)
#         start_handler = CommandHandler('start', start)
#         input_handler = MessageHandler(filters.Text(), game_bot.users_input)

#         #application.add_handler(CommandHandler("start", start))
#         application.add_handler(CallbackQueryHandler(button))
#         #application.add_handler(CommandHandler("help", help_command))
        
#         application.add_handler(CallbackQueryHandler(send_main_menu, pattern="home"))

#         # Create a custom command filter for the '//restart' command
#         # custom_restart_filter = CustomCommandFilter(command='restart', prefix='//')
#         # restart_handler = MessageHandler(custom_restart_filter, game_bot.scheduled_results)


#         application.add_handler(start_handler)
#         application.add_handler(input_handler)
#         # application.add_handler(restart_handler)
        
#         scheduler.start()

#         loop.run_until_complete(application.run_polling())


if __name__ == "__main__":
    run_bot()