import sqlite3
from telegram import Bot
import asyncio

# Replace 'YOUR_BOT_TOKEN' with your Telegram Bot API token
bot_token = '6448511488:AAG2fVNBd7GSd-FdqyNWCSsVve1NowtMKBg'

# Initialize the Telegram Bot
bot = Bot(token=bot_token)

# Connect to the SQLite database
conn = sqlite3.connect('referral2.db')
cursor = conn.cursor()

# Retrieve all Telegram IDs from the database
cursor.execute('SELECT telegram_id FROM users')
telegram_ids = [row[0] for row in cursor.fetchall()]
print("Telegram IDs:", telegram_ids)

# Message to send to all Telegram IDs
#message = "Hello Liquid Player,The FINAL Session of Liquid Game Week 24/11 is still ongoing. Play now to increase your chances of winning tomorrow!"
#message = "Hello Liquid Player. Monday settlement price is 25! Visit https://t.me/liquidmarkets for the Top 50 leaderboard and scores. Congratulations to the winners! Read the updated terms and conditions, See you next week!!" 
#message = "Hello Liquid Player,The first Session of Liquid Game Week 15/12 will kick off at 0800 GMT!.Visit liquidmarkets.org/liquidgame and read the new instructions for the Game and get yourself ready to play."
message = "Hello Liquid Player; The Liquid Game of Week 12 Jan has started!!"


# Send the message to all Telegram IDs
async def send_message_to_tg(telegram_id):
    try:
        await bot.send_message(chat_id=telegram_id, text=message)
        print(f"Message sent to Telegram ID {telegram_id}")
    except Exception as e:
        print(f"Failed to send message to Telegram ID {telegram_id}: {str(e)}")

async def send_message_to_tg_id():
    for telegram_id in telegram_ids:
        await send_message_to_tg(telegram_id)


if __name__ == '__main__':
    asyncio.run(send_message_to_tg_id())
# Close the database connection
conn.close()