import logging
import sqlite3
from telegram import Update, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CommandHandler, MessageHandler, filters,ContextTypes, ConversationHandler, CallbackContext, ApplicationBuilder,CallbackQueryHandler

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)

# Registration states
CODE_VERIFICATION, USERNAME, REGISTERED, HAVE_CODE, NO_CODE = range(5)

# Create or connect to an SQLite database
conn6 = sqlite3.connect('referral2.db')
cursor6 = conn6.cursor()

# Create a table to store user registration data
cursor6.execute('''CREATE TABLE IF NOT EXISTS users
                  (user_id INTEGER PRIMARY KEY, telegram_id INTEGER, username TEXT)''')
                  
cursor6.execute('''CREATE TABLE IF NOT EXISTS referrals
                  (id INTEGER PRIMARY KEY, user_id INTEGER, referral_code STRING, referral_count INTEGER, referred_by INTEGER)''')


cursor6.execute('''CREATE TABLE IF NOT EXISTS plays
                  (game INTEGER PRIMARY KEY, user_id INTEGER, score FLOAT, username TEXT)''')
                  

#cursor.execute('''CREATE TABLE IF NOT EXISTS new_plays
#                  (id INTEGER PRIMARY KEY,game INTEGER, user_id INTEGER, score FLOAT, username TEXT, num_plays INTEGER)''')
                  
#cursor.execute("INSERT INTO new_plays (game, user_id,score,username, num_plays) SELECT game, user_id,score,username, num_plays from plays")

#cursor.execute("ALTER TABLE result_summary ADD COLUMN total_energy VARCHAR(255)")
#cursor6.execute('''UPDATE referrals SET referral_count = 0 ''')
#cursor.execute("ALTER TABLE new_plays RENAME TO plays")
  

conn6.commit()

# User data dictionary to temporarily store registration information
user_data = {}

# Command to start registration
async def start(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    user_id = user.id  # Get the Telegram user ID
    # Check if the user is registered in the database
    cursor6.execute("SELECT telegram_id FROM users WHERE telegram_id=?", (user_id,))
    existing_user = cursor6.fetchone()
    
    #conn6.commit()
    
    # User is already registered, send a button to play the game
    keyboard = [[InlineKeyboardButton("Play the game", url="https://t.me/liquid24bot")]]
    reply_markup = InlineKeyboardMarkup(keyboard)
    
    keyboard2 = [[
        InlineKeyboardButton("YES", callback_data="have_code"),
        InlineKeyboardButton("NO", callback_data="no_code"),
    ],]
    
    reply_markup2 = InlineKeyboardMarkup(keyboard2)
    
    if existing_user:
        cursor6.execute("SELECT referral_count FROM referrals WHERE user_id=?", (user_id,))
        referral_data = cursor6.fetchone()
        
        if referral_data and referral_data[0] >= 0:
            await update.message.reply_text(f"Welcome back, {user.first_name}! You are already registered to play in this week's Liquid Game.\n Click the button below to play the Liquid Game.", reply_markup=reply_markup)
            return REGISTERED
    cursor6.execute("INSERT INTO referrals (user_id, referral_code, referral_count, referred_by) VALUES (?, ?, ?, ?)", (user_id, user_id, 0, user_id))
    conn6.commit()
    await update.message.reply_text(f"Hi {user.first_name}! Welcome to Liquid Game Week 22/12.")
    await update.message.reply_text("Do you have the Referral/Verification Code?", reply_markup=reply_markup2)
    #await update.message.reply_text("Before we proceed, please accept the rules of the game by typing 'YES'")
    user_data[user_id] = {'telegram_id': user_id}  # Store the Telegram user ID
    return CODE_VERIFICATION

async def button(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:

    async def have_code(update, context):
        user_id = update.callback_query.from_user.id
        context.user_data[user_id] = {}
        await context.bot.send_message(user_id,"Please enter the Referral/Verification Code:")
        user_data[user_id] = {'telegram_id': user_id}
        return CODE_VERIFICATION
        
        
    async def no_code(update: Update, context: CallbackContext):
        user_id = update.callback_query.from_user.id
        keyboard = [[InlineKeyboardButton("Play Game", url = "https://t.me/liquid24bot")]]
        reply_markup = InlineKeyboardMarkup(keyboard)
        
        user = update.callback_query.from_user
        
        user_data = context.user_data.get(user_id, {})
        
        cursor6.execute("INSERT INTO users (user_id, telegram_id, username) VALUES (?, ?, ?)", (user_id, user_id, user.first_name))
        conn6.commit()
        
        await context.bot.send_message(user_id, "You don't have the Referral Code!. Play the Liquid Game by Clicking the button below.", reply_markup=reply_markup)
        return NO_CODE
        
    query = update.callback_query
    
    await query.answer()
    
    if query.data =="have_code":
        await have_code(update, context)
    elif query.data =="no_code":
        await no_code(update, context)
    
    
async def verify_code(update: Update, context: CallbackContext) -> int:
    user_id = update.message.from_user.id
    code = update.message.text.strip()
    
    conn6.execute("BEGIN TRANSACTION")
    #check if the code exists in the db
    cursor6.execute("SELECT * FROM referrals WHERE referral_code=?", (code,))
    existing_referral = cursor6.fetchone()
    #print(existing_code)
    
    if existing_referral:
        used_by = str(existing_referral[4])  # Assuming used_by is the fifth column

        # Update the used_by field to include the new user ID
        if used_by:  # Check if used_by is not empty
            used_by_list = used_by.split(',')  # Split the string into a list
            used_by_list.append(str(user_id))  # Append the new user ID
            used_by_updated = ','.join(used_by_list)  # Convert the list back to a string
        else:
            used_by_updated = str(user_id)  # If used_by is empty, set it to the new user ID

        # Increment the referral_count and update the used_by field for the given referral_code
        cursor6.execute("UPDATE referrals SET referral_count = referral_count + 1, referred_by = ? WHERE referral_code = ?", (used_by_updated, code))
        # Commit the changes to the database
        conn6.commit()
        
        user_data[user_id].update({'referral_code': code})
        await update.message.reply_text("Code verified successfully! Please reply with 'YES' to accept game rules")
        return USERNAME
    else:
        url = "https://t.me/liquid24bot"
        await update.message.reply_text(f"Invalid code. Please enter a valid code or head on to {url} and share the game with friends using 'Referrals' button to get the verification code.")
        return CODE_VERIFICATION
# Store the username and Telegram ID in the database and user_data
async def receive_username(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    user_id = user.id  # Get the Telegram user ID
    username = update.message.text

    # Check if the telegram_id already exists in the database
    cursor6.execute("SELECT telegram_id FROM users WHERE telegram_id=?", (user_id,))
    
    # User is already registered, send a button to play the game
    keyboard = [[InlineKeyboardButton("Play the game", url="https://t.me/liquid24bot")]]
    reply_markup = InlineKeyboardMarkup(keyboard)
    
    existing_user = cursor6.fetchone()
    if existing_user:

        await update.message.reply_text(f"Welcome back, {user.first_name}! You are already registered.\n Click the button below to play the Liquid Game 22/12.", reply_markup=reply_markup)
        return REGISTERED
    else:
        # Store the username and Telegram ID in the database
        
        cursor6.execute("INSERT INTO users (user_id, telegram_id, username) VALUES (?, ?, ?)", (user_id, user_id, user.first_name))
        conn6.commit()

        # Store the username and Telegram ID in user_data
        user_data[user_id].update({'username': user.first_name})

        # Reply with a confirmation message
        await update.message.reply_text(f"Thanks, {user.first_name}! You are now registered for Liquid Game 22/12 and able to win a challenge.\n Click the button below to play the Liquid Game.", reply_markup=reply_markup)
        return REGISTERED

# End the conversation
async def end_registration(update: Update, context: CallbackContext) -> int:
    user = update.message.from_user
    user_id = user.id  # Get the Telegram user ID
    del user_data[user_id]  # Remove user data

    # If the user is not already registered, send a registration successful message
    if 'username' in user_data[user_id]:
        await update.message.reply_text("Registration successful!.")
    
    return ConversationHandler.END

# Start the bot
def main():
    token = "6448511488:AAG2fVNBd7GSd-FdqyNWCSsVve1NowtMKBg"

    application = ApplicationBuilder().token(token).build()

    # Create a conversation handler for registration
    registration_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start)],
        states={
            CODE_VERIFICATION: [MessageHandler(filters.Text() & ~filters.Command(), verify_code)],
            USERNAME: [MessageHandler(filters.Text() & ~filters.Command(), receive_username)],
            REGISTERED: [MessageHandler(filters.Text() & ~filters.Command(), end_registration)],
        },
        fallbacks=[],
    )
    
    application.add_handler(CallbackQueryHandler(button))
    
    # Add the conversation handler to the dispatcher
    application.add_handler(registration_handler)

    # Start the Bot
    application.run_polling()

if __name__ == '__main__':
    main()
    
    
    
    
    
    
    
    
